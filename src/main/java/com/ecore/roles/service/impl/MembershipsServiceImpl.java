package com.ecore.roles.service.impl;

import com.ecore.roles.client.model.Team;
import com.ecore.roles.exception.InvalidArgumentException;
import com.ecore.roles.exception.InvalidObjectException;
import com.ecore.roles.exception.ResourceExistsException;
import com.ecore.roles.exception.ResourceNotFoundException;
import com.ecore.roles.model.Membership;
import com.ecore.roles.model.Role;
import com.ecore.roles.repository.MembershipRepository;
import com.ecore.roles.repository.RoleRepository;
import com.ecore.roles.service.MembershipsService;
import com.ecore.roles.service.TeamsService;
import com.ecore.roles.service.UsersService;
import lombok.NonNull;
import lombok.RequiredArgsConstructor;
import lombok.extern.log4j.Log4j2;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.Optional;
import java.util.UUID;

@RequiredArgsConstructor
@Log4j2
@Service
public class MembershipsServiceImpl implements MembershipsService {

    private final MembershipRepository membershipRepository;
    private final RoleRepository roleRepository;
    private final TeamsService teamsService;
    private final UsersService usersService;

    public Membership assignRoleToMembership(@NonNull Membership membership) {
        validateMembership(membership);
        return membershipRepository.save(membership);
    }

    private void validateMembership(Membership membership) {
        UUID roleId = Optional.ofNullable(membership.getRole())
                .map(Role::getId)
                .orElseThrow(() -> new InvalidArgumentException(Role.class));

        roleRepository.findById(roleId)
                .orElseThrow(() -> new ResourceNotFoundException(Role.class, roleId));

        Optional.ofNullable(teamsService.getTeam(membership.getTeamId()))
                .orElseThrow(() -> new ResourceNotFoundException(Team.class, membership.getTeamId()));

        Optional.ofNullable(usersService.getUser(membership.getUserId()))
                .orElseThrow(() -> new InvalidObjectException(Membership.class,
                        "The provided user doesn't belong to the provided team."));

        if (this.findByUserIdAndTeamId(membership.getUserId(), membership.getTeamId())
                .isPresent()) {
            throw new ResourceExistsException(Membership.class);
        }
    }

    public List<Membership> getMembershipsByRoleId(@NonNull UUID roleId) {
        return membershipRepository.findByRoleId(roleId);
    }

    public Optional<Membership> findByUserIdAndTeamId(UUID userId, UUID teamId) {
        return membershipRepository.findByUserIdAndTeamId(userId, teamId);
    }
}
