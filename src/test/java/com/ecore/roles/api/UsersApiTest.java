package com.ecore.roles.api;

import com.ecore.roles.client.model.User;
import com.ecore.roles.utils.RestAssuredHelper;
import com.ecore.roles.web.dto.UserDto;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.web.server.LocalServerPort;
import org.springframework.test.web.client.MockRestServiceServer;
import org.springframework.web.client.RestTemplate;

import static com.ecore.roles.utils.MockUtils.mockGetUserById;
import static com.ecore.roles.utils.MockUtils.mockGetUsers;
import static com.ecore.roles.utils.RestAssuredHelper.getUser;
import static com.ecore.roles.utils.RestAssuredHelper.getUsers;
import static com.ecore.roles.utils.TestData.GIANNI_USER;
import static com.ecore.roles.utils.TestData.GIANNI_USER_UUID;
import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest(webEnvironment = SpringBootTest.WebEnvironment.RANDOM_PORT)
public class UsersApiTest {

    private final RestTemplate restTemplate;

    private MockRestServiceServer mockServer;

    @LocalServerPort
    private int port;

    @Autowired
    public UsersApiTest(RestTemplate restTemplate) {
        this.restTemplate = restTemplate;
    }

    @BeforeEach
    void setUp() {
        mockServer = MockRestServiceServer.createServer(restTemplate);
        RestAssuredHelper.setUp(port);
    }

    @Test
    void shouldGetUserById() {
        User expectedUser = GIANNI_USER();
        mockGetUserById(mockServer, GIANNI_USER_UUID, GIANNI_USER());
        UserDto actualUsers = getUser(GIANNI_USER_UUID)
                .statusCode(200)
                .extract().as(UserDto.class);

        assertThat(actualUsers.getId()).isNotNull();
        assertThat(actualUsers).isEqualTo(UserDto.fromModel(expectedUser));
    }

    @Test
    void shouldGetAllUsers() {
        mockGetUsers(mockServer, GIANNI_USER());
        UserDto[] users = getUsers()
                .statusCode(200)
                .extract().as(UserDto[].class);

        assertThat(users.length).isEqualTo(1);
        assertThat(users[0].getId()).isNotNull();
    }
}
